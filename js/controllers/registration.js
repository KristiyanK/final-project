app.controller('RegistrationController', ['$scope', 'Authentication',
	function ($scope, Authentication) {

		$scope.login = function () {
			// Call to login from Authentication.js
			Authentication.login($scope.user);
		};

		$scope.logout = function () {
			// Call to logout from Authentication.js
			Authentication.logout();
		};

		$scope.register = function () {
			// Call to register from Authentication.js
			Authentication.register($scope.user);
		}; //register
}]); //Controller