//FIXME: This document should be merged inside the app.js
var myApp = angular.module('myApp', ['ngRoute', 'firebase']);

app.run(['$rootScope', '$location', function ($rootScope, $location) {
	$rootScope.$on('$routeChangeError', function (event, next, previous, error) {
		if (error == 'AUTH_REQUIRED') {
			$rootScope.message = 'Sorry, you must log in to access that page';
			$location.path('/login');
		} //Auth Required
	}); //$routeChangeError
}]); //run