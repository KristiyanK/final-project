//app has been declared in quiz.js    
app.controller ( 'english-grammar-controller', [
    '$scope', '$http', '$sce', '$timeout',
    function ( $scope, $http, $sce, $timeout )
    {
        
        // displays the % correct answers at the end of the test.
        $scope.precentage = 0;
        
        //Arrays that hold the index of the questions.
        var rightAnswers = [];
        var wrongAnswers = [];
        var questionsArrayIndexes = [];
		$scope.randomTest= [];
		var arrayLengthCounter = 10;
		//Controls the size of the array
		var dbSize = null;

		// TODO: Increase the question array size via variable
        function randomiser(questionsArray)
        {	
			while(questionsArray.length < 10)
            {
				// Math.random return a number between 0 and 1 (multiply to maxQuestions in DB)
                var randomnumber = Math.ceil(Math.random() * 25);
                if(questionsArray.indexOf(randomnumber) > -1) continue;
                questionsArray[questionsArray.length] = randomnumber;
            }
			console.log("questionsArray");
			console.log(questionsArray);
            return questionsArray;
			
			//add return function (needed for the promise)
        };
		
		       
        // qIndex = Question Index && aIndex = Answer Index
        $scope.selectAnswer = function (qIndex, aIndex)
        {
			// NOTE: Adds the questionState in the JSON notation			
			var questionState = $scope.randomTest[qIndex].questionState;
            
            if (questionState != 'answered')
            {
                $scope.randomTest[qIndex].selectedAnswer = aIndex;   
                var correctAnswer = $scope.randomTest[qIndex].correct;
                $scope.randomTest[qIndex].correctAnswer = correctAnswer;
                
                // if the given answer matches the answer from DB push qIndex into rightAnswers[]
                if (aIndex === correctAnswer)
                {
                    $scope.randomTest[qIndex].correctness = 'correct';
                    $scope.score += 1;
					// FIXME: Cannot read property 'push' of undefined
                    rightAnswers.push(qIndex);
					console.log(rightAnswers);
                }
                else
                {
                    $scope.randomTest[qIndex].correctness = 'incorrect';
                    wrongAnswers.push(qIndex);
                }
                //NOTE: Do not remove! Keeps the user from exploiting the score system. Without this line the user can click multiple times on the same answer and increase their score. 
                $scope.randomTest[qIndex].questionState = 'answered';
            }
            //Calculates the precentage right questions with accuracy of 1 decimal points.
            $scope.precentage = (($scope.score / $scope.totalQuestions) * 100).toFixed(1);
            
        };
        
        //Returns true if the selected answer is same as index answer.
        $scope.isSelected = function (qIndex, aIndex){
            
			/*
            console.log("qIndex " + qIndex);
            console.log(aIndex);
            */
			
            return $scope.randomTest[qIndex].selectedAnswer === aIndex;
            
            console.log(qIndex);
            console.log(aIndex);
        };
        
        // Check if the given answer is correct or not
        $scope.isCorrect = function (qIndex, aIndex){
            return $scope.randomTest[qIndex].correctAnswer === aIndex;
        };
        
        // attach to button at the end of test (records test)
        $scope.submitTest = function(){
            console.log("Test submitted!");
        }
		
		$scope.buttonClick = function(index){
			console.log(index);
		}
		
// MAIN LOGIC

        // NOTE: Getting the JSON game data
        //Logs success message and record the data
        $http.get('data/quiz_data.json')
			.then(function(quizData)
			{
				$scope.myQuestions = quizData.data;
				// NOTE: Total amount of questions in the DB	
				$scope.totalQuestions = $scope.myQuestions.length;
			return $scope.totalQuestions;
		})
			.then(
			function(){
				//console.log(this.dbSize);		
				randomiser(questionsArrayIndexes, $scope.totalQuestions)}
			)
			.then(function(questionsArray)
		    {
				// FIXME: change using .Map object
				questionsArrayIndexes.forEach(function(qIndex){
				qIndex--;
				$scope.randomTest.push($scope.myQuestions[qIndex]);
				console.log(qIndex);
			});// http.get

		}); // forEach
	}
]);