/** Created by Kristiyan Kovachev based on Razvan Dinita - 26/09/2016. */

app.controller (
	'NavigationController', [
		'$scope', function ( $scope )
		{

			// JSON notation
			$scope.navItems = [
				{
					link: '/register',
					title: 'Register'
				},
				{
					link: '/login',
					title: 'Login'
				},
                {
                    link: '/profile',
                    title: 'Profile'
                }
			];

		}
	]
);