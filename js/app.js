var app = angular.module('myQuiz', ['ngRoute', 'ngAnimate', 'firebase']);

app.config(
	[
		'$routeProvider',
		function ($routeProvider)
        {
                var defaultRoute = '/login'; 

            

            // ROUTE CONFIGURATION - WHEN USER ACCESSES THE FOLLOWING LINKS, LOAD THE APPROPRIATE VIEWS / PAGES
			$routeProvider
				.when('/landing', {
					templateUrl: 'views/landing.html'
				})
				.when('/register', {
					templateUrl: 'views/register.html',
					controller: 'RegistrationController'
				})
				.when('/login', {
					templateUrl: 'views/login.html',
					controller: 'RegistrationController'
				})
                .when('/profile',{
                    templateUrl: 'views/profile.html',
                    controller: 'RegistrationController'
                })
                .when('/english-grammar-test',{
                      templateUrl: 'views/english-grammar-test.html',
                      controller: 'english-grammar-controller'
                })
                // IF THE USER REQUESTS AN UNRECOGNISED LINK, REDIRECT THEM TO THE DEFAULT ONE
                .otherwise({
                    redirectTo: defaultRoute
                });
		}
	]
);

//Returns the user to the /login page if they are not authenticated i.e. registered
app.run(['$rootScope', '$location', function ($rootScope, $location) {
	$rootScope.$on('$routeChangeError', function (event, next, previous, error) {
		if (error == 'AUTH_REQUIRED') {
			$rootScope.message = 'Sorry, you must log in to access that page';
			$location.path('/login');
		} //Auth Required
	}); //$routeChangeError
}]); //run