app.factory('Authentication', ['$rootScope', '$location', '$firebaseObject', '$firebaseAuth',
  function ($rootScope, $location, $firebaseObject, $firebaseAuth) {
        
	  	//Firebase defaults
		var ref = firebase.database().ref();
		var auth = $firebaseAuth();
		var myObject;

		auth.$onAuthStateChanged(function (authUser) {
			if (authUser) {
				var userRef = ref.child('users').child(authUser.uid);
				var userObj = $firebaseObject(userRef);
				$rootScope.currentUser = userObj;
			} else {
				$rootScope.currentUser = '';
			}
		});

	  	//Authenticating user
		myObject = {
			login: function (user) {
				auth.$signInWithEmailAndPassword(
					user.email,
					user.password
				).then(function (user) {
					$location.path('/profile');
				}).catch(function (error) {
					$rootScope.message = error.message;
				}); //signInWithEmailAndPassword
			}, //login

			logout: function () {
				return auth.$signOut();
			}, //logout

			requireAuth: function () {
				return auth.$requireSignIn();
			}, //require Authentication

			// Registration of users
			register: function (user) {
				auth.$createUserWithEmailAndPassword(
					user.email,
					user.password
				).then(function (regUser) {
					// sending data to Firebase to be saved & catching any errors
					var regRef = ref.child('users')
						.child(regUser.uid).set({
							date: firebase.database.ServerValue.TIMESTAMP,
							regUser: regUser.uid,
							firstname: user.firstname,
							lastname: user.lastname,
							email: user.email
						}); //userinfo
					myObject.login(user);
				}).catch(function (error) {
					$rootScope.message = error.message;
				}); //createUserWithEmailAndPassword
			} //register

		}; //return
		return myObject;
}]); //factory